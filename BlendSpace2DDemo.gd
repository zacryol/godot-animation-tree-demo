extends Node2D

func _process(delta: float) -> void:
	$AnimationTree.set("parameters/blend_position", Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down"))
