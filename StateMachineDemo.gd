extends Node2D

onready var state_machine = $AnimationTree.get("parameters/playback")

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept"):
		state_machine.travel("punch")
